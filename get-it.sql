-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2018 at 03:52 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `get-it`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_admin` varchar(30) NOT NULL,
  `role` enum('Admin') NOT NULL,
  `foto` varchar(100) NOT NULL,
  `first_login` datetime NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`, `nama_admin`, `role`, `foto`, `first_login`, `last_login`) VALUES
('nais', '$2y$12$nSN9LUKvhFkG8iOWt5tzIu3o.585T99UjrSpMnWQQCTKj8iPCW8Ni', 'Mochamad Fajar Sodik', 'Admin', 'files/profil_admin/2018/July/onta.jpg', '2018-07-19 00:00:00', '2018-07-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `atm_getit`
--

CREATE TABLE `atm_getit` (
  `id` varchar(100) NOT NULL,
  `bank` varchar(25) NOT NULL,
  `no_rekening` varchar(50) NOT NULL,
  `atas_nama` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` varchar(75) NOT NULL,
  `nama_kategori` varchar(35) NOT NULL,
  `slug` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `konten_web`
--

CREATE TABLE `konten_web` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `konten` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` varchar(50) NOT NULL,
  `tipe` enum('F','P') NOT NULL,
  `email` varchar(80) NOT NULL,
  `idfb` int(11) NOT NULL,
  `idgo` int(11) NOT NULL,
  `token` int(11) NOT NULL,
  `pass_rec` int(11) NOT NULL,
  `pass_val` int(11) NOT NULL,
  `nama` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `first_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `member_provider`
--

CREATE TABLE `member_provider` (
  `id_provider` mediumtext NOT NULL,
  `id_member` varchar(255) NOT NULL,
  `nama_provider` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating_provider`
--

CREATE TABLE `rating_provider` (
  `id_rating` varchar(100) NOT NULL,
  `id_member_given` varchar(100) NOT NULL,
  `id_provider_received` varchar(100) NOT NULL,
  `rate` int(11) NOT NULL,
  `description` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `atm_getit`
--
ALTER TABLE `atm_getit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konten_web`
--
ALTER TABLE `konten_web`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating_provider`
--
ALTER TABLE `rating_provider`
  ADD PRIMARY KEY (`id_rating`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
