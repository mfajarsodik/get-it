- bayar bakal masuk ke get it dulu abis itu baru ke provider
- passwordnya mantap
- menu :
  - profile :
    - setting profile
    - hapus akun
  - member :
    - list member
    - hapus akun
    - aktivasi manual
  - provider :
    - list provider
    - hapus akun
    - aktivasi manual
    - provider premium
    - provider pro
    - provider rating
    - payment provider
  - technician :
    - list technician
    - hapus akun
    - aktivasi manual
    - technician premium
    - technician pro
    - technician rating
    - payment technician
  - web content :
    - meta tag
    - icon
  - maintenance :
    - to do list
