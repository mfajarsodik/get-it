<?php
/**
 *
 */
class M_Admin extends CI_Model
{

  public function getDataUser($username = '')
  {
    if ($username == NULL) {
      return $this->db->get('admin');
    } else {
      $this->db->where('username', $username);
      $result = $this->db->get('admin');
      if ($result->num_rows() > 0) {
        return $result->row();
      } else {
        return false;
      }
    }
  }

  public function updateData($tabel,$data,$where = ''){
    return $this->db->update($tabel,$data,$where);
  }

  public function deleteData($tabel,$where = ''){
    return $this->db->delete($tabel,$where);
  }

}

?>
