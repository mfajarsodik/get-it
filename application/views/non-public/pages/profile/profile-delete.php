<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('kepoin/mantap') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>G-IT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b> Get-IT</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- bar notification put this -->

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/non-public/dist/img/user2-160x160.jpg') ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('nama'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/non-public/dist/img/user2-160x160.jpg'); ?>" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('kepoin/mantap/profile/detail/' . $this->session->userdata('username')); ?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('kepoin/mantap/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="<?php echo base_url('kepoin/mantap/'); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-address-card-o"></i>
            <span>Data Profile</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="<?php echo base_url('kepoin/mantap/profile/detail/' . $this->session->userdata('username')); ?>"><i class="fa fa-circle-o"></i> Setting Profile</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Hapus Akun</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-o"></i>
            <span>Member</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>List Member</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Hapus Akun</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Aktivasi Manual</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Provider</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>List Provider</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Hapus Akun</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Aktivasi Manual</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Provider Premium</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Provider Pro</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Provider Rating</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Payment Provider</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-circle"></i>
            <span>Technician</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>List Technician</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Hapus Akun</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Aktivasi Manual</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Technician Premium</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Technician Pro</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Payment Technician</a></li>
          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Profile
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('kepoin/mantap'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Ubah Data Profil</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="submit">
              <div class="box-body">
                <div class="form-group">
                  <label for="username">Username</label>
                  <input name="username" type="text" class="form-control" id="username" placeholder="Username" value="<?php echo $profile_data->username; ?>" disabled>
                </div>
                <div class="form-group">
                  <label for="nama_admin">Nama Admin</label>
                  <input type="text" class="form-control" name="nama_admin" value="<?php echo $profile_data->nama_admin; ?>">
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input name="password" type="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                  <label for="repassword">Repassword</label>
                  <input type="password" class="form-control" name="repassword" placeholder="Repassword">
                </div>

                <div class="form-group">
                  <label for="foto_profil">Upload Foto Profil</label>
                  <input name="foto_profil" type="file" id="foto_profil">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url("$profile_data->foto") ?>" alt="User profile picture">

              <h3 class="profile-username text-center">Foto Profil Anda</h3>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2018 <a href="http://sparkish.co.id">PT Sparkish Kinosoft Technology</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<script type="text/javascript">
  $(document).ready(function () {

    $('#submit').submit(function (e) {
      e.preventDefault();
      // $.ajax({
      //   url         : '<?php echo base_url('kepoin/mantap/profile/edit/' . $this->uri->segment(5)) ?>',
      //   type        : 'post',
      //   data        : new FormData(this),
      //   processData : false,
      //   contentType : false,
      //   cache       : false,
      //   async       : false,
      //   success     : function (data) {
      //     alert("Upload Image Berhasil");
      //   }
      //   error       : function () {
      //     alert("upload gagal");
      //   }
      // });
    });
  });
</script>
