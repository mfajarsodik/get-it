<script src="<?php echo base_url('assets/public/js/jquery-1.11.3.min.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/owl.carousel.min.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/nivo-lightbox.min.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/jquery.animateNumber.min.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/parallax.min.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/jquery.appear.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/index-script.js') ?>"></script>
<script src="<?php echo base_url('assets/public/js/common-script.js') ?>"></script>
</body>
</html>
