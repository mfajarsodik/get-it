<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keyword" content="your keyword goes to here">
    <meta name="author" content="Sparkish Kinosoft Technology">
    <title>GetIt - Your Solution for Software & web development, IT Maintenance and SEO</title>
    <link href="<?php echo base_url('assets/public/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/public/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/public/css/owl.carousel.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/public/css/owl.theme.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/public/css/nivo-lightbox.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/public/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/public/css/media-query.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
