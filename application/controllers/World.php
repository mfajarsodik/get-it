<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class World extends CI_Controller {

	public function index()
	{
		$this->load->view('public/templates/header');
    $this->load->view('public/pages/index');
    $this->load->view('public/templates/footer');
	}
}
