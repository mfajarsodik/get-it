<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Sparkish
 */
class Mantap extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_admin');
  }

  public function index()
  {
    $status = $this->session->userdata('role');
    if ($status == "Admin") {
      // $data['title']   = "Dashboard";
      $data = array(
        'title'        => "Dashboard",
        'profile_data' => $this->m_admin->getDataUser($this->session->userdata('username')),
      );
      $this->load->view('non-public/templates/header', $data);
      $this->load->view('non-public/pages/index', $data);
      $this->load->view('non-public/templates/footer');
    } else {
      redirect('kepoin/kepo');
    }
  }

  public function logout()
  {
    $this->session->unset_userdata('username');
    $this->session->unset_userdata('nama');
	  $this->session->unset_userdata('role');
	  $this->index();
  }

  public function profile()
  {
    $status = $this->session->userdata('role');
    if ($status == "Admin") {
      if ($this->uri->segment(4) == 'edit') {
        if ($this->uri->segment(5) != NULL) {
          if ($_POST) {
            $dir = 'files/profil_admin/'.date('Y').'/'.date('F').'/';
            if(!is_dir($dir)){
              mkdir($dir, 0755, true);
            }

            $config['upload_path']   = $dir;
            $config['allowed_types'] = 'jpg|png';
            $config['encrypt_name']  = TRUE;
            $config['detect_mime']   = TRUE;
            $this->upload->initialize($config);
            if ($this->upload->doUpload("file")) {
              $data = array('upload_data' => $this->upload->data() );
              $nama_admin = $this->input->post('nama_admin');
              $password   = password_hash($this->input->post('password'), PASSWORD_BCRYPT) ;
              $image      = $data['upload_data']['file_name'];
              $data       = array(
                'password'   => $password,
                'nama_admin' => $nama_admin,
                'foto'       => $image,
              );
              $result   = $this->m_admin->updateData('admin', $data, array('username' => $this->uri->segment(5)));
            } else {
              $error = array('error' => $this->upload->display_errors() );
            }
          } else {
            $data = array(
              'id'           => $this->uri->segment(5),
              'profile_data' => $this->m_admin->getDataUser($this->uri->segment(5)),
              'title'        => 'Profile Edit',
            );
            $this->load->view('non-public/templates/header', $data);
            $this->load->view('non-public/pages/profile/profile-edit',$data);
            $this->load->view('non-public/templates/footer');
          }
        } else {
          show_404();
        }
      } elseif ($this->uri->segment(4) == 'detail') {
        if ($this->uri->segment(5) != NULL) {
          $data = array(
            'id'           => $this->uri->segment(5),
            'profile_data' => $this->m_admin->getDataUser($this->uri->segment(5)),
            'title'        => 'Profile Detail',
          );
          $this->load->view('non-public/templates/header', $data);
          $this->load->view('non-public/pages/profile/profile-detail',$data);
          $this->load->view('non-public/templates/footer');
        } else {
          show_404();
        }
      }
      else {
        show_404();
      }
    } else {
      redirect('kepoin/kepo');
    }
  }

}

?>
