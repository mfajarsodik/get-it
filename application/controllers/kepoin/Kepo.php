<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepo extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_admin');
  }

	public function index()
	{
    $status = $this->session->userdata('role');
    if ($status == "Admin") {
			redirect('kepoin/mantap');
		} else {
			$this->login();
		}
	}

  public function login()
  {
    $status = $this->session->userdata('role');
    if ($status == "Admin") {
      redirect('kepoin/mantap');
    } else {
      $data['title'] = "Login";
      $this->load->view('non-public/templates/header', $data);
      $this->load->view('non-public/pages/login');
      $this->load->view('non-public/templates/footer');
    }
  }

  public function login_valid()
  {
    $this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      echo "<script>alert('Masukan data dengan benar');</script>";
			$this->login();
		} else {
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $data_user = $this->m_admin->getDataUser($username);
      if ($data_user) {
        if (password_verify($password, $data_user->password)) {
          $data = array (
            'username' => $data_user->username,
            'nama'     => $data_user->nama_admin,
            'role'     => $data_user->role
          );
          $this->session->set_userdata($data);
          redirect('kepoin/mantap');
        } else {
          echo "<script>alert('Username / password salah')</script>";
          $this->login();
        }
      } else {
        echo "<script>alert('Data tidak ditemukan')</script>";
        $this->login();
      }
    }
  }
}
